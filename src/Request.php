<?php

namespace Lliure\Http\Message;

use Psr\Http\Message\MessageInterface;
use Psr\Http\Message\RequestInterface;

class Request implements
    MessageInterface,
    RequestInterface
{
    use MessageTrait;
    use RequestTrait;
}