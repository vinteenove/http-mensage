<?php

namespace Lliure\Http\Message;

use Psr\Http\Message\StreamInterface;

trait StreamFactoryTrait{

    protected  $stream = null;

    /**
     * @inheritDoc
     */
    public function createStream(string $content = ''): StreamInterface{
        // TODO: Implement createStream() method.
    }

    /**
     * @inheritDoc
     */
    public function createStreamFromFile(string $filename, string $mode = 'r'): StreamInterface{
        // TODO: Implement createStreamFromFile() method.
    }

    /**
     * @inheritDoc
     */
    public function createStreamFromResource($resource): StreamInterface{
        // TODO: Implement createStreamFromResource() method.
    }
}