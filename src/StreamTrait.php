<?php


namespace Lliure\Http\Message;


trait StreamTrait{

    /** @var resource */
    protected $stream;

    /** @var int|null */
    protected ?int $size;

    /** @var bool */
    protected bool $seekable;

    /** @var bool */
    protected bool $readable;

    /** @var bool */
    protected bool $writable;

    /** @var string|null */
    protected ?string $uri;

    /** @var mixed[] */
    protected array $customMetadata;

    abstract public function __construct($stream, array $options = []);

    /** @inheritDoc */
    public function __toString(){
        try{
            if ($this->isSeekable()) {
                $this->seek(0);
            }
            return $this->getContents();
        }catch(\Throwable $e){
            return false;
        }
    }

    /** @inheritDoc */
    public function close(){
        try{
            if ($this->hasStream()){

                if (is_resource($this->stream)) {
                    fclose($this->stream);
                }

                $this->detach();
            }
        }

        catch(\RuntimeException $e){}
    }

    /** @inheritDoc */
    public function detach(){
        try{
            $this->hasStream();
        }catch(\RuntimeException $e){
            return null;
        }

        $result = $this->stream;
        unset(
            $this->stream,
            $this->size,
            $this->uri,
            $this->readable,
            $this->writable,
            $this->seekable
        );

        return $result;
    }

    /** @inheritDoc */
    public function getSize(): ?int{

        if ($this->size ?? false) {
            return $this->size;
        }

        try{
            $this->hasStream();
        }catch(\RuntimeException $e){
            return null;
        }

        // Clear the stat cache if the stream has a URI
        if ($this->uri ?? false){
            clearstatcache(true, $this->uri);
        }

        $stats = fstat($this->stream);
        if (is_array($stats) && isset($stats['size'])){
            return $this->size = $stats['size'];
        }

        return null;

    }

    /** @inheritDoc */
    public function tell(){
        $this->hasStream();

        if ($result = ftell($this->stream) === false){
            throw new \RuntimeException('Unable to determine stream position');
        }

        return $result;
    }

    /** @inheritDoc */
    public function eof(){
        $this->hasStream();

        return feof($this->stream);
    }

    /** @inheritDoc */
    public function isSeekable(){
        return $this->seekable;
    }

    /** @inheritDoc */
    public function seek($offset, $whence = SEEK_SET){
        $this->hasStream();

        $whence = (int) $whence;

        if (!$this->seekable) {
            throw new \RuntimeException('Stream is not seekable');
        }

        if (fseek($this->stream, $offset, $whence) === -1) {
            throw new \RuntimeException(sprintf('Unable to seek to stream position %s with whence %s', $offset, var_export($whence, true)));
        }

    }

    /** @inheritDoc */
    public function rewind(){
        $this->seek(0);
    }

    /** @inheritDoc */
    public function isWritable(){
        return $this->writable;
    }

    /** @inheritDoc */
    public function write($string){
        $this->hasStream();

        if (!$this->isWritable()) {
            throw new \RuntimeException('Cannot write to a non-writable stream');
        }

        // We can't know the size after writing anything
        $this->size = null;
        $result = fwrite($this->stream, $string);

        if ($result === false) {
            throw new \RuntimeException('Unable to write to stream');
        }

        return $result;
    }

    /** @inheritDoc */
    public function isReadable(){
        return $this->readable;
    }

    /** @inheritDoc */
    public function read($length){

        $this->hasStream();

        if (!$this->isReadable()) {
            throw new \RuntimeException('Cannot read from non-readable stream');
        }
        if ($length < 0) {
            throw new \RuntimeException('Length parameter cannot be negative');
        }

        if (0 === $length) {
            return '';
        }

        $string = fread($this->stream, $length);
        if (false === $string) {
            throw new \RuntimeException('Unable to read from stream');
        }

        return $string;
    }

    /** @inheritDoc */
    public function getContents(){
        $this->hasStream();

        $contents = stream_get_contents($this->stream);

        if ($contents === false) {
            throw new \RuntimeException('Unable to read stream contents');
        }

        return $contents;

    }

    /** @inheritDoc */
    public function getMetadata($key = null){
        try{
            $this->hasStream();
        }catch(\RuntimeException $e){
            return $key ? null : [];
        }

        $metaData = $this->customMetadata + stream_get_meta_data($this->stream);

        if (!$key) {
            return $metaData;
        }

        return $metaData[$key] ?? null;
    }

    public function hasStream(): bool{
        if(!isset($this->stream)){
            throw new \RuntimeException('Stream is detached');
        }
        return true;
    }
    
}