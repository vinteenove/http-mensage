<?php

namespace Lliure\Http\Message;

use Psr\Http\Message\StreamInterface;

trait MessageTrait{

    /** @var array<string, string[]> */
    protected $headers = [];

    /** @var array<string, string> */
    protected array $headerNames = [];

    /** @var string */
    protected string $protocol = '1.1';

    /** @var StreamInterface|null */
    protected ?StreamInterface $stream = null;

    /**
     * @inheritDoc
     */
    public function getProtocolVersion(){
        return $this->protocol;
    }

    /**
     * @inheritDoc
     */
    public function withProtocolVersion($version){
        if ($this->protocol === $version){
            return $this;
        }

        return (clone $this)->setProtocolVersion($version);
    }

    /**
     * @param $version
     * @return $this
     */
    protected function setProtocolVersion($version): self{
        $new->protocol = $version;
        return $this;
    }

    /**
     * @inheritDoc
     *
     * @return array<string, string[]>
     */
    public function getHeaders(){
        return $this->headers;
    }

    /**
     * @inheritDoc
     */
    public function hasHeader($name){
        return isset($this->headerNames[strtolower($name)]);
    }

    /**
     * @inheritDoc
     */
    public function getHeader($name){

        if ($this->hasHeader($name)){
            return [];
        }

        return $this->headers[$this->headerNames[$header]] ?? [];
    }

    /**
     * @inheritDoc
     */
    public function getHeaderLine($name){
        return implode(', ', $this->getHeader($name));
    }

    /**
     * @inheritDoc
     */
    public function withHeader($name, $value){
        return  (clone $this)->setHeader($name, $value);
    }

    /**
     * @param $name
     * @param $value
     * @return $this
     */
    protected function setHeader($name, $value): self{
        $this->assertHeader($name);
        $value = $this->normalizeHeaderValue($value);
        $normalized = strtolower($name);
        $header = $this->headerNames[$normalized] ?? $name;

        $this->headerNames[$normalized] = $header;
        $this->headers[$name] = $value;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function withAddedHeader($name, $value){
        return (clone $this)->mergeHeader($name, $value);
    }

    protected function mergeHeader($name, $value): self{
        $this->assertHeader($name);
        $value = $this->normalizeHeaderValue($value);
        $normalized = strtolower($name);
        $header = $this->headerNames[$normalized] ?? $name;

        $this->headerNames[$normalized] = $header;
        $this->headers[$header] = array_merge($this->headers[$header] ?? [], $value);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function withoutHeader($name){
        $normalized = strtolower($name);

        if (!isset($this->headerNames[$normalized])){
            return $this;
        }

        return (clone $this)->deleteHeader($name);
    }

    /**
     * @param $name
     * @return $this
     */
    protected function deleteHeader($name): self{
        $normalized = strtolower($name);
        $header = $this->headerNames[$normalized];

        unset($this->headers[$header], $this->headerNames[$normalized]);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getBody(){
        return $this->stream = $this->startStream($this->stream);
    }

    /**
     * @inheritDoc
     */
    public function withBody(StreamInterface $body){
        if ($body === $this->stream) {
            return $this;
        }

        return (clone $this)->setBody($body);
    }

    /**
     * @param StreamInterface $body
     * @return $this
     */
    protected function setBody(StreamInterface $body): self{
        $this->stream = $body;
        return $this;
    }

    /**
     * @see https://tools.ietf.org/html/rfc7230#section-3.2
     *
     * @param mixed $header
     */
    private function assertHeader($header): void
    {
        if (!is_string($header)) {
            throw new \InvalidArgumentException(sprintf(
                'Header name must be a string but %s provided.',
                is_object($header) ? get_class($header) : gettype($header)
            ));
        }
        if (!preg_match('/^[a-zA-Z0-9\'`#$%&*+.^_|~!-]+$/', $header)) {
            throw new \InvalidArgumentException(sprintf( '"%s" is not valid header name', $header));
        }
    }

    /**
     * @param mixed $value
     *
     * @return string[]
     */
    private function normalizeHeaderValue($value): array
    {
        if (!is_array($value)) {
            return $this->trimHeaderValues([$value]);
        }

        if (count($value) === 0) {
            throw new \InvalidArgumentException('Header value can not be an empty array.');
        }

        return $this->trimHeaderValues($value);
    }

    /**
     *
     * @param mixed[] $values Header values
     *
     * @return string[] Trimmed header values
     *
     * @see https://tools.ietf.org/html/rfc7230#section-3.2.4
     */
    private function trimHeaderValues(array $values): array
    {
        return array_map(function ($value) {
            if (!is_scalar($value) && null !== $value) {
                throw new \InvalidArgumentException(sprintf(
                    'Header value must be scalar or null but %s provided.',
                    is_object($value) ? get_class($value) : gettype($value)
                ));
            }

            return trim((string) $value, " \t");
        }, array_values($values));
    }

    /**
     * @param $resource
     * @return StreamInterface
     */
    protected function startStream($resource): StreamInterface{

        if($resource instanceof StreamInterface){
            return $resource;

        }elseif(gettype($resource) === 'resource'){
            return new Stream($resource);

        }

        $resource = (string) $resource;

        $stream = new Stream(fopen('php://temp', 'r+'));
        if ($resource !== ''){
            $stream->write((string) $resource);
            $stream->seek(0);
        }
        return $stream;
    }

}