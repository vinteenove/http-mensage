<?php

namespace Lliure\Http\Message;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

/**
 * PSR-7 response implementation.
 */
trait ResponseTrait
{
    use MessageTrait;

    /** @var string */
    private string $reasonPhrase;

    /** @var int */
    private int $statusCode;

    /**
     * @param int                                  $status  Status code
     * @param array                                $headers Response headers
     * @param string|resource|StreamInterface|null $body    Response body
     * @param string                               $version Protocol version
     * @param string|null                          $reason  Reason phrase (when empty a default will be used based on the status code)
     */
    public function __construct(
        int $status = 200,
        array $headers = [],
        $body = null,
        string $version = '1.1',
        string $reason = null
    ) {

        $this->setStatus($status, $reason);

        foreach($headers as $name => $value){
            $this->setHeader($name, $value);
        }

        $this->stream = $this->startStream($body);

        $this->protocol = $version;
    }

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    public function getReasonPhrase(): string
    {
        return $this->reasonPhrase;
    }

    public function withStatus($code, $reasonPhrase = ''): ResponseInterface
    {
        return (clone $this)->setStatus((int) $this->assertStatusCodeRange($this->assertStatusCodeIsInteger($code)), $reasonPhrase);
    }

    /**
     * @param        $code
     * @param string $reasonPhrase
     * @return $this
     */
    protected function setStatus($code, $reasonPhrase = ''): self{
        $this->statusCode = $code;

        if ($reasonPhrase == '' && isset(Response::PHRASES[$this->statusCode])) {
            $reasonPhrase = Response::PHRASES[$this->statusCode];
        }

        $this->reasonPhrase = (string) $reasonPhrase;
        return $this;
    }

    /**
     * @param mixed $statusCode
     * @return int
     */
    private function assertStatusCodeIsInteger($statusCode): int
    {
        if (filter_var($statusCode, FILTER_VALIDATE_INT) === false) {
            throw new \InvalidArgumentException('Status code must be an integer value.');
        }

        return (int) $statusCode;
    }

    /**
     * @param int $statusCode
     * @return int
     */
    private function assertStatusCodeRange(int $statusCode): int
    {
        if ($statusCode < 100 || $statusCode >= 600) {
            throw new \InvalidArgumentException('Status code must be an integer value between 1xx and 5xx.');
        }

        return $statusCode;
    }
}